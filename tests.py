# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 by Luciano Camargo Cruz <luciano@lccruz.net>, Joao Toss Molon <jtmolon@gmail.com>
#
# GNU General Public License (GPL)
#
import unittest
from analisador_lexico import AnalisadorLexico

class TestAnalisador(unittest.TestCase):
    
    def setUp(self):
        self.analisador = AnalisadorLexico()

    def test_arquivo(self):
        self.assertEqual(self.analisador.testaArquivo("tests/fonte.c"),"tests/fonte.c")
    
    def test_operadores(self):
        result =[{'lexema': '==', 'linha': 3, 'coluna': 1, 'id': 2},
                {'lexema': '=', 'linha': 3, 'coluna': 3, 'id': 1},
                {'lexema': '!=', 'linha': 3, 'coluna': 4, 'id': 8},
                {'lexema': '<', 'linha': 3, 'coluna': 6, 'id': 5},
                {'lexema': '=', 'linha': 3, 'coluna': 7, 'id': 1},
                {'lexema': '>', 'linha': 3, 'coluna': 8, 'id': 3},
                {'lexema': '!', 'linha': 3, 'coluna': 9, 'id': 7},
                {'lexema': '!=', 'linha': 3, 'coluna': 10, 'id': 8},
                {'lexema': '&', 'linha': 3, 'coluna': 12, 'id': 9},
                {'lexema': '&', 'linha': 3, 'coluna': 13, 'id': 9}]
        self.assertEqual(self.analisador.analise("===!=<=>!!=&&",3),result)

    def test_numeros(self):
        result =[{'lexema': '12343', 'linha': 1, 'coluna': 1, 'id': 75},
                {'lexema': '4567', 'linha': 1, 'coluna': 7, 'id': 75}]
        self.assertEqual(self.analisador.analise("12343 4567",1),result)
    
    def test_letras(self):
        result =[{'lexema': 'assb', 'linha': 1, 'coluna': 1, 'id': 77},
                {'lexema': 'ppoi', 'linha': 1, 'coluna': 6, 'id': 77}]
        self.assertEqual(self.analisador.analise("assb ppoi",1),result)

    def test_ope_num_letras(self):
        result=[{'lexema': 'asdf', 'linha': 1, 'coluna': 1, 'id': 77},
                {'lexema': '==', 'linha': 1, 'coluna': 5, 'id': 2},
                {'lexema': '=', 'linha': 1, 'coluna': 7, 'id': 1},
                {'lexema': '!', 'linha': 1, 'coluna': 8, 'id': 7},
                {'lexema': 'hah1234___aha', 'linha': 1, 'coluna': 9, 'id': 77},
                {'lexema': '=', 'linha': 1, 'coluna': 22, 'id': 1},
                {'lexema': '1', 'linha': 1, 'coluna': 24, 'id': 75},
                {'lexema': 'aba', 'linha': 1, 'coluna': 26, 'id': 77}]
        self.assertEqual(self.analisador.analise("asdf===!hah1234___aha= 1 aba",1),result)
    
    def test_reservada(self):
        result =[{'lexema': '2', 'linha': 1, 'coluna': 1, 'id': 75},
                {'lexema': 'int', 'linha': 1, 'coluna': 3, 'id': 45},
                {'lexema': '2.3', 'linha': 1, 'coluna': 7, 'id': 76},
                {'lexema': 'float', 'linha': 1, 'coluna': 11, 'id': 46},
                {'lexema': 'struct', 'linha': 1, 'coluna': 17, 'id': 54},
                {'lexema': 'dio', 'linha': 1, 'coluna': 24, 'id': 77},
                {'lexema': 'for', 'linha': 1, 'coluna': 28, 'id': 62},
                {'lexema': 'i', 'linha': 1, 'coluna': 32, 'id': 77},
                {'lexema': 'if', 'linha': 1, 'coluna': 34, 'id': 63},
                {'lexema': '0', 'linha': 1, 'coluna': 37, 'id': 75},
                {'lexema': 'else', 'linha': 1, 'coluna': 39, 'id': 64},
                {'lexema': 'break', 'linha': 1, 'coluna': 44, 'id': 70},
                {'lexema': 'return', 'linha': 1, 'coluna': 50, 'id': 73}]
        self.assertEqual(self.analisador.analise("2 int 2.3 float struct dio for i if 0 else break return",1),result)

    def test_nao_reconhece_circunflexo(self):
        self.assertFalse(self.analisador.analise("iasdf 23 4 5^34",1))

    def test_fonte_true(self):
        arquivo_fonte_saida = open("tests/fonte_saida.lex",'r')
        self.analisador.analisaArquivo("tests/fonte.c")
        arquivo_resultado = open("saida.lex", 'r')
        self.assertEqual(arquivo_fonte_saida.readlines(), arquivo_resultado.readlines())
        
if __name__ == '__main__':
    unittest.main()
