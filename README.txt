Analisador Léxico desenvolvido em Python.

Contributors:
    - Luciano Camargo Cruz    <luciano@lccruz.net>
    - Joao Toss Molon         <jtmolon@gmail.com>

License: 
    GPL 

Requisitos:
    Python >= 2.6

Rodar App:
    Analisador Lexico:
        python main_lexico.py        
        pode passar o arquivo que esta em tests/fonte.c como exemplo.

    Analisador Sintatico:
        python main_sintatico.py
        pode passar o arquivo que esta em tests/fonte_sintatico.c como exemplo.

Rodar Test:
    python tests.py
