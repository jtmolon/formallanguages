# -*- coding: utf-8 -*-
#
# Copyright (c) 2012 by Luciano Camargo Cruz <luciano@lccruz.net>, Joao Toss Molon <jtmolon@gmail.com>
#
# GNU General Public License (GPL)
#
from analisador_lexico import AnalisadorLexico

analisador = AnalisadorLexico()
nome_arquivo = raw_input("Digite o nome do arquivo que será analisado: ")
analisador.analisaArquivo(nome_arquivo)
